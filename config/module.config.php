<?php
/**
 * OBADJA - Frontend Application for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsApp;

return array(
    'router' => array(
        'routes' => array(
            'widget' => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '#'
                ],
            ]
        ),
    ),
    'service_manager' => array(
        'allow_override' => true,
        'invokables' => array(
            'Javascript' => 'MellorsApp\Service\Javascript',
        ),          
        'factories' => array( 
            'Zend\Session\SessionManager' => function($serviceLocator){
                $config = new \Zend\Session\Config\SessionConfig();
                $config->setRememberMeSeconds(60 * 60 * 24 * 30);
                $config->setGcMaxlifetime(60 * 60 * 24 * 30);
                $sessionManager = new \Zend\Session\SessionManager($config);
                $sessionManager->rememberMe();
                return $sessionManager;
            },
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
            'app_navigation' => 'MellorsApp\Navigation\Service\AppNavigationFactory',
            'actionService' => function($serviceLocator) {
                $actionService = new \MellorsApp\Service\ActionService();
                $actionService->setRouter($serviceLocator->get('router'));
                $actionService->setConfig(new \MellorsApp\Actions\Config());
                return $actionService;
            },
            'url' => function($serviceLocator) {
                $urlService = new \MellorsApp\Service\Url();
                $urlService->setRouteMatch($serviceLocator->get('Application')->getMvcEvent()->getRouteMatch());
                $urlService->setJavascriptRenderer($serviceLocator->get('javascript'));
                return $urlService;
            },
            'formhandler' => function($serviceLocator){
                $formHandler = new \MellorsApp\Service\FormHandler();
                $request = $serviceLocator->get('Request');
                if($request instanceof \Zend\Http\Request) {
                    $formHandler->setRequest($request);
                }
                return $formHandler;
            },
            'asset_path' => function($serviceLocator){
                $config = $serviceLocator->get('config');
                $env = isset($config['application_env']) ? $config['application_env'] : 'production';
                $pathes = array(
                    'production' => '/build',
                    'development' => '/dev'
                );
                return $pathes[$env];
            }
        ),
        'aliases' => array(
            //Fix https://github.com/ZF-Commons/ZfcUserDoctrineORM/issues/54
            'zfcuser_doctrine_em' => 'Doctrine\ORM\EntityManager',
            'Zend\Authentication\AuthenticationService' => 'zfcuser_auth_service'
         )

    ),
    'controllers' => array(
        'initializers' => array(
            'Access' => 'MellorsApp\Controller\Initializer\Access',
            'ActionService' => 'MellorsApp\Controller\Initializer\ActionService',
            'View' => 'MellorsApp\Controller\Initializer\View'            
        )
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'ControllerName' => 'MellorsApp\Controller\Plugin\ControllerName',
            'EntityManager' => 'MellorsApp\Controller\Plugin\EntityManager'            
        )   
    ),
    'view_helpers' => array(
        'invokables' => array(
            'AssetUrl' => 'MellorsApp\View\Helper\AssetUrl',            
            'Module' => 'MellorsApp\View\Helper\Module',
            'ShowForm' => 'MellorsApp\View\Helper\ShowForm',        
            'FormElement' => 'MellorsApp\View\Helper\FormElement',        
            'FormLabel' => 'MellorsApp\View\Helper\FormLabel',
            'Attribute' => 'MellorsApp\View\Helper\Attribute',
            'Route' => 'MellorsApp\View\Helper\Route',
        ),
        
        'factories' => array(
            'Http' => function($helperPluginManager){
                $application = $helperPluginManager->getServiceLocator()->get('Application');
                $helper = new \MellorsApp\View\Helper\Http();
                $helper->setResponse($application->getResponse());        
                $helper->setRequest($application->getRequest());        
                return $helper;
            },
            'Actions' => function($helperPluginManager){
                $helper = new \MellorsApp\View\Helper\Actions();
                $service = $helperPluginManager->getServiceLocator()->get('actionservice');
                $helper->setActionService($service);
                return $helper;
            }
        )

    ),
                
    // Doctrine config
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Model/Entity')
            ),
          
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Model\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        )
    ),
    'view_manager' => array(
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_path_stack' => array(
            __DIR__ . '/../view',
        )
    ),

    'form_elements' => [
        'initializers' => [
            'FormHandlerInitializer' => function ($element, $formElements) {
                if(!$element instanceof \Zend\Form\Form) return;
                $services = $formElements->getServiceLocator();
                $formhandler = $services->get('formhandler');
                $formhandler->addForm($element);
            }
        ]
    ],

    'navigation' => array(
        'default' => array(),
        'app_navigation' => array(),
    ),
    'zfc_rbac' => [
        'role_provider' => [
            'ZfcRbac\Role\InMemoryRoleProvider' => [
                'admin' => [
                    'permissions' => ['admin'],
                    'children'    => ['member']
                ],
                'member' => [
                    'permissions' => ['frontend'],
                    'children'    => ['guest']
                ],
                'guest' => []
            ]
        ],
        'redirect_strategy' => [
            'redirect_to_route_connected' => 'zfcuser/login',
            'append_previous_uri' => true,
            'previous_uri_query_key' => 'redirect'
        ],
        'unauthorized_strategy' => [
            'template' => 'error/403.phtml',
        ],
    ]
);
