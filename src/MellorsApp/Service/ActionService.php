<?php

/**
 * OBADJA - Frontend Application for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsApp\Service;

use \Zend\Mvc\Router\RouteStackInterface as Router;
use \Zend\Http\PhpEnvironment\Request as Request;

class ActionService 
{

    /**
     * action config (dependencies etc.)
     *
     * @var array
     */     
    protected $_config;

    /**
     * saved actions
     *
     * @var array
     */     
    protected $_actions = [];

    /**
     * saved actions
     *
     * @var array
     */     
    protected $_wrappers = [];


    /**
     * Router ro route referer
     *
     * @var Router
     */   
    protected $_router;
   
    /**
     * Referer request
     *
     * @var Request
     */     
    protected $_refererRequest;
    
    /**
     * Set of dependent actions:
     * will be dispatched automatically after their parent action
     *
     * @var array
     */     
    protected $_dependencies = [];
    
    /**
     * The original action of dispatch process
     * (this means not a dependent action)
     *
     * @var array
     */       
    protected $_primaryAction;
    
    
    /**
     * Params of current dispatcing process
     *
     * @var array
     */     
    protected $_params = [];
    
    
    public function setRouter(Router $router)
    {
        $this->_router = $router;
    }
    
    public function getDependencies($actionName = NULL)
    {
        if(!$actionName || !$this->getAction($actionName)) return [];
        $dependencies = [];
        foreach($this->getAction($actionName)->getDependentActions() as $dependentAction){
            if($this->checkDependentAction($dependentAction->name)){
                $dependencies[] = $dependentAction->name;
            }
        }
        return $dependencies;
    }
    
    protected function checkDependentAction($actionName)
    {        
        
        if($this->getAction($actionName) && $this->getRefererParams()){
            if(count($this->getAction($actionName)->getCrucialParams()) == 0) return true;
            foreach($this->getAction($actionName)->getCrucialParams() as $param){
               //Compare Values of the last request
			   if($param == 'controller' || $param == 'module') return false;
               if($this->getParams($param) != $this->getRefererParams($param)) return true;
            }
            return false;
        }
        return true;
    }
    
    public function setPrimaryAction($action)
    {
        $this->_primaryAction = $action;
    }
    
    public function getPrimaryAction()
    {
        return $this->_primaryAction;
    }
    
    public function setRefererUri($refererUri)
    {
        $refererRequest = new Request();
        $refererRequest->setUri($refererUri);  
        $this->_refererRequest = $refererRequest;
    }
    
    public function getRefererParams($param = NULL)
    {
        if(!$this->_router || !$this->_refererRequest){
            return false;
        }
        $refererParams = array();
        
        $refererMatch = $this->_router->match($this->_refererRequest);
        if($refererMatch){
            if($param){
                $refererParams = $refererMatch->getParam($param);           
            }
            else {
                $refererParams = $refererMatch->getParams();
            }
        }
        return $refererParams;
        
    }
    
    public function setParams($params)
    {
        $this->_params = $params;
    }
    
    public function getParams($param = NULL)
    {
        if(!$this->_params) return false;
        if(!$param) return $this->_params;
        
        foreach(['fromRoute', 'fromQuery', 'fromPost'] as $paramType){
            $value = $this->_params->$paramType($param);
            if($value) return $value;
        }
        return false;
    }

    public function setConfig($config)
    {
        $this->_config = $config;
        return $this;
    }

    public function getConfig()
    {
        return $this->_config;
    }

    public function addConfig($config, $callParam = false)
    {

        if(isset($config['display_order'])){
            foreach($config['display_order'] as $wrapper => $actions){
                if(!$this->getWrapper($wrapper)){
                    $this->addWrapper($wrapper);
                }
                foreach($actions as $actionName){
                    if(!$this->getAction($actionName)){
                        $this->addAction($actionName);
                    }
                    $this->getWrapper($wrapper)->appendAction($this->getAction($actionName));
                }
            }
        }

        if(isset($config['dependent_actions'])){
            foreach($config['dependent_actions'] as $action => $dependentActions){
                if(!$this->getAction($action)){
                    $this->addAction($action);
                }
                foreach($dependentActions as $dependentAction){
                    if(!$this->getAction($dependentAction)){
                        $this->addAction($dependentAction);
                    }
                    $this->getAction($action)->addDependentAction($this->getAction($dependentAction));                  
                }
            }
        }

        if(isset($config['ajax_actions'])){
            foreach($config['ajax_actions'] as $action => $ajaxAction){
                if(!$this->getAction($action)){
                    $this->addAction($action);
                }
                if(is_callable($ajaxAction)){
                    $this->getAction($action)->ajaxAction = $ajaxAction($callParam);                                    
                }
                else {
                    $this->getAction($action)->ajaxAction = $ajaxAction;     
                }
            }
        }

        if(isset($config['crucial_params'])){
            foreach($config['crucial_params'] as $action => $params){
                if(!$this->getAction($action)){
                    $this->addAction($action);
                }
                foreach($params as $param){
                    $this->getAction($action)->addCrucialParam($param);                
                }
            }
        }

        return $this;

    }

    public function isInDefaultWrapper($actionName)
    {
        foreach($this->getWrappers() as $wrapper){
            if($wrapper->getActionsByName($actionName)) return false;
        }
        return true;
    }

    public function addWrapper($wrapperName)
    {
        $this->_wrappers[$wrapperName] = new \MellorsApp\Actions\Wrapper();
        return $this;
    }

    public function getWrapper($wrapperName)
    {
        if(!isset($this->_wrappers[$wrapperName])){
            return false;
        }
        return $this->_wrappers[$wrapperName];
    }

    public function getWrappers()
    {
        return $this->_wrappers;
    }

    public function addAction($actionName)
    {
        $this->_actions[$actionName] = new \MellorsApp\Actions\Action($actionName);
        return $this;        
    }

    public function getAction($actionName)
    {
        if(!isset($this->_actions[$actionName])){
            return false;
        }
        return $this->_actions[$actionName];
    }

    public function getActions()
    {
        return $this->_actions;
    }


    public function getAjaxActions()
    {
        $ajaxActions = [];
        foreach($this->getActions() as $action){
            if(!$this->checkDependentAction($action->name)) continue;
            $ajaxActions[$action->name] = $action;
        }
        return $ajaxActions;
    }
        
}

?>
