<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActionService
 *
 * @author Marcel Mellor
 */

namespace MellorsApp\Service;

use Zend\Http\Request;


class FormHandler
{

    public function isSubmitted($form)
    {
        if(isset($this->_request) && $this->_request->getPost($this->getIdentifier($form)) == 'submitted') return true;
        return false;
    }

    public function isValid($form)
    {
        if(!$this->isSubmitted($form)) return false;
        $form->setData($this->_request->getPost());
        if($form->isValid()) return true;
        return false;
    }

    public function setRequest(Request $request)
    {
        $this->_request = $request;
        return $this;
    }

    public function addForm($form)
    {
        $form->add([
            'name' => $this->getIdentifier($form), 
            'attributes' => [
                'type' => 'hidden', 
                'value' => 'submitted'
            ]
        ]); 
    }

    public function getIdentifier($form)
    {
        $id = strtolower(str_replace('\\', '_', get_class($form)));
        return $id . '_submit_hint';
    }


}

?>
