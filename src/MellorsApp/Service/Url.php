<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActionService
 *
 * @author Marcel Mellor
 */

namespace MellorsApp\Service;

use Zend\Http\Request;

class Url
{
    /**
     * @var array URL config which info for routes etc.
     */
    protected $_urlConfig = array();
    
    /**
     * @var array persist all parsed URLs to avoid duppletts
     */
    protected $_parsedUrls = array();
    
    /**
     * @var \MellorsApp\Service\Javascript 
     */
    protected $_jsRenderer;
    
    
    public function __construct() 
    {
        $this->clear();
    }
    
    /**
     * @param \MellorsApp\Service\Javascript $jsRenderer
     */
    public function setJavascriptRenderer(\MellorsApp\Service\Javascript $jsRenderer)
    {
        $this->_jsRenderer = $jsRenderer;
    }
    
    public function clear()
    {
        $this->_urlConfig = array(
            'url' => false,
            'params' => array(),
            'userParams' => array(),
            'identifier' => false,
            'parentContainer' => false,
        );

        return $this;
    }
    
    /**
     *
     * @param string $url
     * @return \MellorsApp\Service\Url
     * @todo URL validation via Zend_URL
     */
    public function setUrl($url)
    {
        $this->_urlConfig['url'] = $url;
        return $this;
    }
    
    /**
     * Set params
     * 
     * @param array $params
     * @return \MellorsApp\Service\Url
     */
    public function setParams($params)
    {   
        foreach($params as $key => $value){
            $params[$key] = strtolower($value);
            if(preg_match('/^:/', $value)){
                unset($params[$key]);
                $this->_urlConfig['userParams'][] = $key;
            }
        }
        $this->_urlConfig['params'] = $params;
        return $this;
    }
    
    /**
     * Get all params
     * 
     * @return mixed
     */
    public function getAjaxParams()
    {
        
        $params = array_intersect_key($this->_urlConfig['params'], array_flip(array('action', 'controller', 'module')));        

        if(!isset($params['controller'])){
            $splitted = explode("\\", $this->getRouteMatch()->getParam('controller'));
            $params['controller'] = strtolower(end($splitted));
        }

        if(!isset($params['module']) && $this->getRouteMatch()){
            $params['module'] = $this->getRouteMatch()->getParam('module');
        }
        return $params;
    }

    /**
     * Get all params
     * 
     * @return mixed
     */
    public function getParams()
    {
        return $this->_urlConfig['params'];
    }

    public function setRouteMatch($routeMatch)
    {
        $this->_routeMatch = $routeMatch;
        return $this;
    }

    public function getRouteMatch()
    {
        return $this->_routeMatch;
    }
    
    /**
     * Set CSS selector of HTML-Element, which contains the link (form or anchor)
     * If not set a selector which use the target url will be automatically created
     * 
     * @param string $id
     * @return \MellorsApp\Service\Url
     */
    public function setIdentifier($id = NULL)
    {
        if(!$id){
            if($this->_urlConfig['url']){
                $id = 'a[href="' . $this->_urlConfig['url'] . '"], form[action="' . $this->_urlConfig['url'] . '"]';                
            }
        }
        $this->_urlConfig['identifier'] = $id;
      
        return $this;
    }
    
    /**
     * Set CSS selector of HTML-Element, which contains the link element
     * If not set, body will be automatically set as parent element
     * 
     * @param string $container
     * @return \MellorsApp\Service\Url
     */
    public function setParentContainer($container = NULL)
    {
        if(!$container){
            $container = 'body';
        }
        $this->_urlConfig['parentContainer'] = $container;
      
        return $this;
    }
   
    /**
     * Create javascript for ajax URL and persist this javascript
     */
    protected function addUrl()
    {
        if(!$this->_urlConfig['identifier']){
            $this->setIdentifier();
        }
        if(!$this->_urlConfig['parentContainer']){
            $this->setParentContainer();
        }
       $urlID = $this->_urlConfig['parentContainer'] . "-" . $this->_urlConfig['identifier'];
       if(!isset($this->_parsedUrls[$urlID])){
            $script = 
                     //"console.log(document.querySelectorAll('" . $this->_urlConfig['identifier'] . "'));" .
                     "jj.trigger('ajaxlink', {".
                        //link element
                        "el : (document.querySelectorAll('" . $this->_urlConfig['identifier'] . "')),"
                        //urlParams 
                        . (( 'urlParams : ' . json_encode($this->getAjaxParams()) . ',')) 
                        //userParams
                        . ((count($this->_urlConfig['userParams']) > 0 ) ? ( 'userParams : ' . json_encode($this->_urlConfig['userParams']) . ',') : ('')) . 
                        //parentContainer
                        'parentContainer : "' . $this->_urlConfig['parentContainer'] . '"
                     }, {state:true});';
            $this->_parsedUrls[$this->_urlConfig['parentContainer'] . "-" . $this->_urlConfig['identifier']] = 'SET';
            $this->_jsRenderer->append($script);#
       }
    }
    
    public function process() 
    {
        $this->addUrl();
        $this->clear();
        
        return $this;
    }
}

?>
