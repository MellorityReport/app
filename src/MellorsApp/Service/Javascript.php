<?php

/**
 * OBADJA - Frontend MellorsApplication for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsApp\Service;

class Javascript
{
    
    /**
     * Persists all generated javascripts in an array 
     * 
     * @var array 
     */
    protected $_scripts = '';
    
    /**
     * Defines the request type of current dispatch
     * 
     * @var string 
     */
    protected $_requestType = 'http';
    
    /**
     * Defines the intended order of script in _scripts
     * @var type 
     */
    protected $_order = array();
  
    /**
     * 
     * @param string $script
     * @param string $id
     * @return \App\Service\Javascript
     */
    public function append($script, $id = false)
    {
        if(!$id){
            $id = rand(0, 5000);
        }
        if(!isset($this->_scripts[$id])){
            $this->_order[] = $id;           

             if(is_array($script)){
                 $script = $this->encodeArray($script);
             }
             //echo " === " . $script . " === ";
             $this->_scripts[$id] = $script;
        }
        return $this;
    }
    
    /**
     * Returns rendered script
     * @return string
     */
    public function getScript(){
        return $this->renderScript();
    }

    /**
     * @param string $type Allowed values are "http" and "xmlHttp"
     */
    public function setRequestType($type)
    {
        $this->_requestType = $type;
    }
    
    /**
     * 
     * @param array $array
     * @return string
     */
    protected function encodeArray($array)
    {
         $script = '';
         foreach($array as $key => $values){
             $script .= "jj.set('" . $key . "', " . 
                     \Zend\Json\Json::encode($values, false, array('enableJsonExprFinder' => true)) . 
                     ");"; 
        }
        return $script;

    }
    
    /**
     * Merge existing snippets amd return rendered script
     * 
     * @return string
     */
    protected function renderScript()
    {
        $string = "requirejs(['jj'],function   (jj) {";
            foreach($this->_order as $id){
                $string .= $this->_scripts[$id];
            }
            $string .= "jj.trigger('setuppage', jj.get('params'), jj.get('title'), {state: true});});";
        return $string;
    }
}

?>
