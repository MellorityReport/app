<?php

namespace MellorsApp\Navigation\Service;

use Zend\Navigation\Service\DefaultNavigationFactory;

class AppNavigationFactory extends DefaultNavigationFactory {

    protected function getName() {
        return 'app_navigation';
    }

}
?>
