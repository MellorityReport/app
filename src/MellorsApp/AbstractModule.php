<?php
/**
 * OBADJA - Frontend Application for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsApp;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceLocatorAwareInterface; 

abstract class AbstractModule extends \ZfcBase\Module\AbstractModule implements ServiceLocatorAwareInterface
{
    
    private $_serviceLocator;

    public function onBootstrap(MvcEvent $event)
    {
        $eventManager        = $event->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);        
    }

    public function bootstrap(\Zend\ModuleManager\ModuleManager $moduleManager, \Zend\Mvc\ApplicationInterface $app){
        $this->setServiceLocator($app->getServiceManager());
        parent::bootstrap($moduleManager, $app);
    }

    public function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator){
        $this->_serviceLocator = $serviceLocator;
        return $this;
    }

    public function getServiceLocator(){
        return $this->_serviceLocator;
    }
    
}
