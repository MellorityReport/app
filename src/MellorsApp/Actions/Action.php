<?php

/**
 * OBADJA - Frontend Application for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsApp\Actions;

class Action 
{

	public $name;

	public $ajaxAction = 'replace';

	public $dependentActions = [];

	public $crucialParams = [];

	public function __construct($name = false, $dependentActions = false)
	{
		if($name){
			$this->name = $name;
		}
		if($dependentActions){
			$this->dependentActions = $dependentActions;
		}
		return $this;
	}

	public function addDependentAction(Action $action)
	{
		$this->dependentActions[$action->name] = $action;
		return $this;
	}

	public function getDependentActions()
	{
		return $this->dependentActions;
	}

	public function addCrucialParam($param)
	{
		$this->crucialParams[] = $param;
		return $this;
	}

	public function getCrucialParams()
	{
		return $this->crucialParams;
	}

}

?>