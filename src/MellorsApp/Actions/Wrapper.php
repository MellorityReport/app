<?php

/**
 * OBADJA - Frontend Application for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsApp\Actions;

class Wrapper 
{

	public $actions = [];

	public function getActions()
	{
		return $this->actions;
	}

	public function getActionNames()
	{
		return array_map(function($action){
			return $action->name;
		}, $this->actions);
	}

	public function getActionsByName($name)
	{
		return array_filter($this->actions, function($action) use($name){
			return ($action->name == $name);
		});
	}

	public function prependAction(Action $action)
	{
		array_unshift($this->actions, $action);
	}

	public function appendAction(Action $action)
	{
		$this->actions[] = $action;
	}


}

?>