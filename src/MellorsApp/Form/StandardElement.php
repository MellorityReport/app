<?php

namespace MellorsApp\Form;

use Zend\Form\Element;
use Zend\InputFilter\InputProviderInterface;
use Zend\Validator;
use Zend\I18n\Validator\Alpha;

class StandardElement extends Element implements InputProviderInterface
{
    protected $attributes = array(
        'type' => 'text',
    );

    public function getInputSpecification()
    {
        
        return array(

            'name' => $this->getName(),
            'filters' => array(
                array('name' => 'Zend\Filter\StringTrim')
            ),
            'validators' => array(
               array(
                    'name' => 'NotEmpty'
                ),
            ),
        );
    }
}

?>