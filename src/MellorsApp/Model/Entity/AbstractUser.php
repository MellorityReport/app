<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace MellorsApp\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;


class AbstractUser extends \ZfcUser\Entity\User implements \ZfcRbac\Identity\IdentityInterface
{
    
    /**
    * @ORM\Column(type="integer")
    */
    protected $role_id;
    
    /**
    * @ORM\OneToOne(targetEntity="MellorsApp\Model\Entity\Role")
     **/
    protected $role;
    
    public function getRoles() 
    {
        if($this->role){
            return array($this->role->getName());
        }
        else {
            if($this->__get('password') == 'facebookToLocalUser'){
                return array('member');                
            }
            else {
                return array('anonymous');
            }
        }
    }
    
    public function __get($property)
    {
        return $this->$property;
    }
    
    public function __set($property, $value)
    {
        $this->$property = $value;
    }    
}

?>
