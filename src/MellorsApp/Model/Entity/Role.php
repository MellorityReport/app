<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace MellorsApp\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity */

class Role extends AbstractEntity
{
    /**
    * @ORM\Id
    * @ORM\Column(type="integer");
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;
    
        /**
    * @ORM\Column(type="string")
    */
    protected $name;
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getTitle()
    {
        return $this->name;
    }
    
}

?>
