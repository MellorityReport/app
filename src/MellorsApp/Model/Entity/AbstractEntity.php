<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace MellorsApp\Model\Entity;


abstract class AbstractEntity 
{
    
    /**
    * Magic getter to expose protected properties.
    *
    * @param string $property
    * @return mixed
    */
    public function __get($property)
    {
        $getMethod = 'get'. $this->camelize($property);
        if(method_exists($this, $getMethod)){
            return $this->$getMethod();
        }
        else {      
            return isset($this->$property) ? $this->$property : '';
        }   
    }

    /**
    * Magic setter to save protected properties.
    *
    * @param string $property
    * @param mixed $value
    */
    
    public function __set($property, $value)
    {
        $getMethod = 'set'.$this->camelize($property);
        if(method_exists($this,$getMethod)){
             $this->$getMethod($value);
        }
        else {      
             $this->$property = $value;
        }
        return $this;
    }

    /**
    * Convert the object to an array.
    *
    * @return array
    */
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    /**
    * Magic method for APIs requesting by "set*" and "get*"
    *
    * @param string $property
    * @param mixed $value
    */
    public function __call($name, $arguments) {
        preg_match('/^(set|get)(.+)/', $name, $matches);
        if($matches){
            $property = $this->decamelize($matches[2]);
            if($matches[1] == 'set'){
                $this->$property = $arguments[0];
            }
            else {
                return $this->$property;
            }
        }
    }
    
    public function getImageFromBody($config = []){

        $defaultConfig = ['thumbnail' => false, 'imageoffset' => 0];
        $config = array_merge($defaultConfig, $config);
        $string = '<div>' . trim($this->body) . '</div>';
        $dom = new \DOMDocument;
        $dom->loadHTML($string);
        $imgs = $dom->getElementsByTagName("img");
        if($imgs->length > $config['imageoffset']){
            $img = $imgs->item($config['imageoffset'])->getAttribute("src");
            if($config['thumbnail']) {
                $img = str_replace('media', 'media/thumbnails', $img);
            }
            return $img;
        }
        return false;
    }

    public function decamelize($word) {
        return preg_replace(
            '/(^|[a-z])([A-Z])/e', 
            'strtolower(strlen("\\1") ? "\\1_\\2" : "\\2")',
            $word 
        ); 
    }

    public function getId(){
        return $this->__call('getId', false);
    }

    function camelize($word) { 
        return preg_replace('/(^|_)([a-z])/e', 'strtoupper("\\2")', $word); 
    }
    
}

?>
