<?php
/**
 * OBADJA - Frontend Application for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsApp\Controller\Plugin;

use Zend\Mvc\Controller\AbstractController;

class ControllerName extends \Zend\Mvc\Controller\Plugin\AbstractPlugin
{

    /**
     * Returns the simple name of a controller without namespaces
     * If no name is given, the current controller of dispatching process is used
     * 
     * @param string $controllerName
     * @return string
     */
    public function __invoke($controllerName = NULL) 
    {
        $controller = $this->getController();
        
        if(!$controllerName){   
            $controllerName = get_class($controller);
        }
        $controllerName = strtolower($controllerName);
        $controllerName = str_replace(array(strtolower($controller->params('__NAMESPACE__')), 'controller', '\\'), '', $controllerName);
        
        return $controllerName;
    }
 
    
   
}
