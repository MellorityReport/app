<?php
/**
 * OBADJA - Frontend Application for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsApp\Controller\Plugin;

use Zend\Mvc\Controller\AbstractController;

class EntityManager extends \Zend\Mvc\Controller\Plugin\AbstractPlugin
{
    /**
     * 
     * @return type
     */
    public function __invoke() 
    {
        return $this->getController()->getServiceLocator()->get('doctrine.entitymanager.orm_default');   
    }
 
}
