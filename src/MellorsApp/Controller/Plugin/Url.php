<?php

/**
 * OBADJA - Frontend Application for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsApp\Controller\Plugin;

use Zend\Mvc\Router\RouteMatch;

class Url extends \Zend\Mvc\Controller\Plugin\Url
{
    /**
     *
     * @var \MellorsApp\Service\Url
     */
    protected $_urlService;
    
    public function fromRoute($route = null, $params = array(), $options = array(), $reuseMatchedParams = false) 
    {
        //Set standard params
        $params['module'] = (isset($params['module']) ? ($params['module']) : $this->getController()->params('module'));
        $params['controller'] = (isset($params['controller']) ? ($params['controller']) : $this->getController()->controllerName());
        
        $this->_urlService->setParams($params);
        if(isset($options['parentContainer'])){
            $this->setParentContainer($options['parentContainer']);                    
        }
        if(isset($options['id'])){
            $this->setIdentifier($options['id']);
        }

        $url = parent::fromRoute($route, $this->_urlService->getParams(), $options, $reuseMatchedParams);
        $url = $this->changeEncoding($url);
        $url = $this->saveCharacters($url);

        $this->_urlService->setUrl($url)->process();
        return $url;
        
    }
    
    /**
     * @param \MellorsApp\Service\Url urlService
     */
    public function setUrlService(\MellorsApp\Service\Url $urlService)
    {
        $this->_urlService = $urlService;
    }
    
    /**
     * Set CSS selector of HTML-Element, which contains the link element
     * If not set, UrlService will set the body as parent
     * Please use this method if possible for perfomance reasons in Javascript
     * 
     * @param string $parentContainer
     * @return \MellorsApp\Controller\Plugin\Url
     */
    public function setParentContainer($parentContainer)
    {
        $this->_urlService->setParentContainer($parentContainer);        
        return $this;
    }
    
    /** 
     * Set CSS selector of HTML-Element, which contains the link (form or anchor)
     * If not set, UrlService will create a selector automatically
     * @param type $id
     * @return \MellorsApp\Controller\Plugin\Url
     */
    public function setIdentifier($id)
    {
        $this->_urlService->setIdentifier($id);
        return $this;
    }

    public function changeEncoding($string){
        //Zend use ISO encoding instead of UTF-8
        $string = str_replace(
            array('%E3%B6', '%E3%BC', '%E3%A4', '%E3%9F', '%E3%FF'),
            array('%C3%B6', '%C3%BC', '%C3%A4', '%C3%9F', '%C3%9F'),
            $string
        );
        return $string;
    }

    public function saveCharacters($string){
        $string = str_replace(
            array('%20'),
            array('+'),
            $string
        );
        return $string;
    }    
   
}
