<?php
/**
 * OBADJA - Frontend MellorsApplication for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsApp\Controller\Initializer;

use Zend\ServiceManager\InitializerInterface;
use MellorsApp\Controller\ViewConfigAware;

class View implements InitializerInterface
{
    
    public function initialize($instance, \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        
        //Attach Javascript String
        $sharedEvents = $serviceLocator->getServiceLocator()->get('application')->getEventManager()->getSharedManager();
                
        $sharedEvents->attach(
            get_class($instance), 
            'dispatch',
            function ($event) {
            
                $controller = $event->getTarget();
                $config = $controller->getServiceLocator()->get('config');

                $layouts = array('frontend', 'mellors-admin');
                $filter = new \Zend\Filter\Word\CamelCaseToDash();

                $module = strtolower($filter->filter($controller->params('module')));
                //Set layout template
                $controller->layout(
                    'layout/html/' . 
                     $module
                ); 
                
                //Set view parameters
                $controller->layout()->setVariables(array(
                    'InlineScript' => $controller->getServiceLocator()->get('Javascript')->getScript(),
                    'controller' => $controller->controllerName(),
                    'remote_debug' => (isset($config['remote_debug'])) ? ($config['remote_debug']) : false,
                    'remote_livereload' => (isset($config['remote_livereload'])) ? ($config['remote_livereload']) : false,
                    'requirejs_config' => $config['application_env'] == 'development' ? 'js/requirejs-config.js' : false,
                    'application_env' => $config['application_env']
                ));
                
                if($controller instanceof ViewConfigAware == false){
                    return false;
                }
        
                if($controller->getRequest()->isXmlHttpRequest()){
                    
                    //Choose json layout, if ajax request
                    $jsonTemplate = str_replace('html', 'json' , $controller->layout()->getTemplate());
                    $controller->layout($jsonTemplate);
                    $controller->getResponse()->getHeaders()->addHeaders(array(
                        'Content-Type' => 'application/json'
                    ));
                    $controller->getServiceLocator()->get('Javascript')->setRequestType('xmlHttp');
                    
                }
                else {
                    //Set Javascript standard inline
                    $controller->getServiceLocator()->get('Javascript')->append(
                        array(
                            'baseUrl' => $event->getRouter()->getBaseUrl(),
                            'params' => array(
                                "action" => strtolower($controller->params('action')),
                                "controller" => strtolower($controller->controllerName()),
                                "module" => strtolower($controller->params('module'))
                            )
                        ), 'params'
                    );
                }

            }
        );
    }
    
}

?>
