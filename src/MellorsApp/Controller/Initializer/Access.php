<?php

/**
 * OBADJA - Frontend Application for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsApp\Controller\Initializer;

use Zend\ServiceManager\InitializerInterface;


class Access implements InitializerInterface
{
    
    public function initialize($instance, \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        if($serviceLocator->getServiceLocator()->has('ZfcRbac\Service\Rbac') && 
           !$serviceLocator->getServiceLocator()->get('ZfcRbac\Service\Rbac')->isGranted(get_class($instance))){
        
            //Attach Javascript String
            $sharedEvents = $serviceLocator->getServiceLocator()->get('application')->getEventManager()->getSharedManager();
            $sharedEvents->attach(
                get_class($instance),
                'dispatch',
                function ($event) {
                    $controller = $event->getTarget();
                    $controller->redirect()->toRoute('frontend')->setStatusCode(301);
                }
             );
        }
    } 
}

?>
