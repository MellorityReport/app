<?php

/**
 * OBADJA - Frontend Application for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsApp\Controller\Initializer;

use Zend\ServiceManager\InitializerInterface;
use MellorsApp\Controller\ActionConfigAware;


class ActionService implements InitializerInterface
{
    
    /**
     * @var \MellorsApp\Service\ActionService
     */
    protected $_actionService;
   
    /**
     * @var string
     */
    protected $_controllerName;
    
    
    public function initialize($instance, \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        
        if($instance instanceof ActionConfigAware == false){
            return false;
        }
        

        $this->_actionService = $serviceLocator->getServiceLocator()->get('ActionService');
        
        $sharedEvents = $serviceLocator->getServiceLocator()->get('moduleManager')->getEventManager()->getSharedManager();
        $sharedEvents->attach(
            $instance,
            'dispatch',
            function ($event) {
                $controller = $event->getTarget();
                $action = $controller->params('action');
                $actionService = $controller->getServiceLocator()->get('ActionService');
                $config = $controller->getActionConfig();
                if(!is_array($config)){
                    $config = [];
                }
                
                if($controller->params('controller')){
                    $controllerName = $controller->params('controller');
                }
                
                //Set params for action service
                if(!$actionService->getParams()){
                    $actionService->setParams($controller->params());
                }

                //Get last request via _Referer
                $refererUri = $controller->getRequest()->isXmlHttpRequest() ? $controller->params()->fromPost('_Referer') : false;
                if(!$actionService->getRefererParams()){
                    if($refererUri != ''){
                        $actionService->setRefererUri($refererUri);
                    }
                }

                //Set dependent actions from the config of controller
                $standardConfig = [
                    'display_order' => ['default' => [$controller->params('action')]]
                ];

                //Set order and style of action display
                //echo print_r(array_merge_recursive($initializer->_standardConfig, $config));
                $actionService->addConfig(array_merge($standardConfig, $config), $controller);
                
  
                //Execute the dependent actions after dispatching the origin action
                if(!$actionService->getPrimaryAction()){
                    //Set actions as primary action
                    $actionService->setPrimaryAction($action);
                    $controller->layout()->primaryAction = $action;

  
                    //Get dependent actions
                    foreach($actionService->getDependencies($action) as $dependentAction){
                        
                        $child = $controller->forward()->dispatch($controllerName, array_merge($controller->params()->fromRoute(), array('action' => $dependentAction)));
                        
                        $controller->layout()->addChild(
                            $child, $dependentAction      
                        );
                    }
                }

            }
        );
    } 
}

?>
