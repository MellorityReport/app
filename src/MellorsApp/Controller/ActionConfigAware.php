<?php

/**
 * OBADJA - Frontend Application for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsApp\Controller;

interface ActionConfigAware 
{ 
    /**
    * Get action config as an array
    *
    * @return array  Following keys will be parsed: dependent_actions, crucial_params, display_order, display_options, ajax_actions
    */       
    public function getActionConfig();
    
}

?>
