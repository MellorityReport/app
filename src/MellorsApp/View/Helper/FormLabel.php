<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * @package   Zend_View
 */

namespace MellorsApp\View\Helper;

/**
 * Helper for minifying stylesheets with crushCSS
 *
 * @package    Zend_View
 * @subpackage Helper
 */

class FormLabel extends \Zend\Form\View\Helper\FormLabel
{    
    
    public function openTag($attributesOrElement = null) {
        return '<dt> ' . parent::openTag($attributesOrElement);
    }
    
    public function closeTag() {
        return parent::closeTag() . '</dt>';
    }
}

?>