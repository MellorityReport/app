<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * @package   Zend_View
 */

namespace MellorsApp\View\Helper;

/**
 * Helper for minifying stylesheets with crushCSS
 *
 * @package    Zend_View
 * @subpackage Helper
 */

class Url extends \Zend\View\Helper\Url
{
    protected $_urlService;
    
    public function __invoke($name = null, $params = array(), $options = array(), $reuseMatchedParams = false) {
       
        $this->_urlService->setParams($params);
        if(isset($options['parentContainer'])){
            $this->setParentContainer($options['parentContainer']);                    
        }
        if(isset($options['id'])){
            $this->setIdentifier($options['id']);
        }
        $url = parent::__invoke($name, $this->_urlService->getParams(), $options, $reuseMatchedParams);
        $url = $this->changeEncoding($url);
        $this->_urlService->setUrl($url)->process();

        if($name === 'widget'){
            return '#' . $params['action'];
        }

        //Get params created from route
        return $url;
       
    }
    
    public function setUrlService($urlService)
    {
        $this->_urlService = $urlService;
    }
    
    public function setParentContainer($parentContainer)
    {
        $this->_urlService->setParentContainer($parentContainer);        
        return $this;
    }
    
    public function setIdentifier($id)
    {
        $this->_urlService->setIdentifier($id);
        return $this;
    }

    public function changeEncoding($string){
        //Zend use ISO encoding instead of UTF-8
        $string = str_replace(
            array('%E3%B6', '%E3%BC', '%E3%A4', '%E3%9F', '%E3%FF'),
            array('%C3%B6', '%C3%BC', '%C3%A4', '%C3%9F', '%C3%9F'),
            $string
        );
        return $string;
    }
}
