<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_View
 * @subpackage Helper
 * @copyright  Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */

/**
 * @namespace
 */
namespace MellorsApp\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Helper for retrieving an asset URL.
 *
 * @package    Zend_View
 * @subpackage Helper
 * @copyright  Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Module extends AbstractHelper
{
    /**
     * Build a html container arround the rendered action content
     * 
     * @param string $actionName
     * @param type $content
     * @return string action with html container
     */
    public function __invoke($actionName, $content = NULL){
        
        $container = '<div id="' . $actionName . '" class="action ';
        if($this->getPrimaryAction() == $actionName){
            $container .= ' active ';
        }
        if(isset($this->getView()->actions->options)){
            if(isset($this->getView()->actions->options[$actionName]["class"])){
                $container .= $this->getView()->actions->options[$actionName]["class"];
            }
        }
        $container .= '">';
        $container .= $content;
        $container .= '</div>';
        $container .= "\n";    
        
        return $container;
    }

    public function getPrimaryAction()
    {
        return $this->getView()->primaryAction;
    }
}
