<?php
/**
 * OBADJA - Frontend Application for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsApp\View\Helper;

use Zend\Filter\Null;
use Zend\View\Helper\AbstractHelper;

class Actions extends AbstractHelper
{
    
    /**
     * Array with action objects
     * 
     * @var array
     */
    protected $_actions = [];

    /**
     * Array with rendered views
     * 
     * @var array
     */
    protected $_views = [];

    /**
     * Render type (possible values: json, html)
     * 
     * @var type 
     */
    protected $_type;


    protected $_wrappers = [];
    
    /**
     * 
     * @param \Zend\View\Model\ViewModel $viewModel
     * @param string $type
     * @return \MellorsApp\View\Helper\Actions
     */
    public function __invoke(\Zend\View\Model\ViewModel $viewModel, $type = 'html')
    {
        $this->_type = $type;
        $this->_views = $this->fetchViews($viewModel);
        $this->_actions = $this->_actionService->getActions();
        $this->_wrappers = $this->_actionService->getWrappers();

        return $this;
    }

    public function setActionService($actionService)
    {
        $this->_actionService = $actionService;
    }

    public function setActions($actions)
    {
        $this->_actions = $actions;
    }

    public function setAjaxActions($actions)
    {
        $this->_ajax_actions = $actions;
    }

    public function setWrappers($wrappers)
    {
        $this->_wrappers = $wrappers;
    }

    protected function fetchViews($viewModel)
    {
        $views = [];
        foreach(array_reverse($viewModel->getChildren()) as $content){
            $actionName =   (($content->captureTo() == 'content') ? ($this->getPrimaryAction()) : ($content->captureTo()));
            $views[$actionName] = $content;
        }
        return $views;
    }

    protected function getActionsByWrapper($wrapperName)
    {
        $actions = [];
        $wrapper = $this->_wrappers[$wrapperName];

        return array_filter($this->getActions(), function($action){
            return ($wrapper->getActionsByName($action));
        });
    }
    
    /**
     * 
     * @param string $actionName
     * @param \Zend\View\Model\ViewModel $content
     * @return string rendered Action
     */
    public function displayAction($actionName, \Zend\View\Model\ViewModel $content = NULL){
        $container = '';
        if($content){
            $container = $this->getView()->render($content);
        }
        if($this->_type == 'html'){
            $container = $this->getView()->module($actionName, $container);
        }
        return $container;
    }
    
    /**
     * Display all rendered actions of current dispatch process
     *
     * @see \MellorsApp\Controller\ActionService
     * @return string
     */
    public function displayWrapper($wrapper = 'default')
    {
		$containers = [];

        if(isset($this->_wrappers[$wrapper]) && $this->_type == 'html'){
           $containers = $this->_wrappers[$wrapper]->getActionNames();
        }
        else if($wrapper == 'default'){
            foreach($this->_views as $actionName => $view){
                if($this->_type != 'html' || $this->_actionService->isInDefaultWrapper($actionName)){
                    $containers[] = $actionName;
                }
            }
        }
		
		if(count($containers) == 0) return false;

        return $this->displayContainers($containers);
    
    }

    public function displayContainers($containers)
    {
        $transformed = [];
        foreach($containers as $actionName){
            if (!isset($this->_views[$actionName])) {
                $this->_views[$actionName] = null;
            }
            $transformed[$actionName] =  $this->displayAction($actionName, $this->_views[$actionName]);
        }
        return $this->_type == 'html' ? $this->displayHTML($transformed) : $this->displayJson($transformed);
    }

    public function displayJson($containers)
    {
        $jsonResult = [];
        $ajaxActions = $this->_actionService->getAjaxActions();
        foreach($containers as $id => $value){
            if(!isset($ajaxActions[$id])) continue;
            $ajax_action = isset($this->_actions[$id]) ? $this->_actions[$id]->ajaxAction : 'replace';
            $jsonResult[] = [
                'title' => (string) strip_tags($this->getView()->headTitle()),
                'id' => $id,
                'content' => ($ajax_action != 'prop') ? $value : false,
                'action' => $ajax_action,
                'data' => ($ajax_action == 'prop') ? $value : false,
            ];
        }
        return json_encode($jsonResult);
    }

    public function displayHTML($containers)
    {
        return implode($containers);

    }

    public function getPrimaryAction()
    {
        return $this->getView()->primaryAction;
    }
    
    public function setType($type){
        $this->_type = $type;
    }
    
}
