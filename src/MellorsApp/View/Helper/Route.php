<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * @package   Zend_View
 */

namespace MellorsApp\View\Helper;
 
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\View\Helper\AbstractHelper;
 
class Route extends AbstractHelper implements ServiceLocatorAwareInterface {
 
private $serviceLocator;
 
 public function __invoke($replacement = false) {
   $routeMatch = $this->
       serviceLocator->
       getServiceLocator()->
       get('Application')->
       getMvcEvent()->
   	    getRouteMatch();

   $routeName = $routeMatch ? $routeMatch->getMatchedRouteName() : '';
   if($replacement){
   		$routeName = str_replace("/", $replacement, $routeName);
   }
   return $routeName;
 }
 
 /**
 * Set service locator
 *
 * @param ServiceLocatorInterface $serviceLocator
 */
 public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
   $this->serviceLocator = $serviceLocator;
 }
 
 /**
 * Get service locator
 *
 * @return ServiceLocatorInterface
 */
 public function getServiceLocator() {
   return $this->serviceLocator;
 }

}