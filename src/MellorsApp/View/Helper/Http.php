<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * @package   Zend_View
 */

namespace MellorsApp\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Http extends AbstractHelper
{
    protected $_statusCode = 200;
    protected $_redirect = false;
    
    public function __invoke() {
       return $this;
    }
    
    public function getStatusCode(){
        return $this->_statusCode;
    }
    
    public function getRedirect(){
        return $this->_redirect;
    }
    
    public function setResponse(\Zend\Http\Response $response){
        $this->_statusCode = $response->getStatusCode();
        return $this;
    }
    
    public function setRequest(\Zend\Http\Request $request){
        $this->_redirect = $request->getQuery('redirect');
        return $this;
    }
    
}
