<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * @package   Zend_View
 */

namespace MellorsApp\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Helper for minifying stylesheets with crushCSS
 *
 * @package    Zend_View
 * @subpackage Helper
 */

class ShowForm extends AbstractHelper
{
    
    public function __invoke(\Zend\Form\Form $form, $urlParams = [])
    {
        $content = '';

        if($form->getAttribute('action') == ''){
            $form->setAttribute('action', $this->getView()->url(
                'widget', $urlParams, ['id' => '#' . $form->getAttribute('id')]
            ));
        }

        $form->prepare();
        $content .= $this->getView()->form()->openTag($form);
        $content .= (($form->getAttribute('description') != '') ? ('<p class="description">' . $form->getAttribute('description') . '</p>') : ('')) ;
        $content .= '<dl>';
        
        foreach($form->getElements() as $element){
            $content .= $this->getView()->formRow($element);
        }
        
        $content .= '</dl>';
        $content .= $this->getView()->form()->closeTag();
        
        return $content;
    }
 
}
