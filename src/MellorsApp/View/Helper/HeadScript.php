<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * @package   Zend_View
 */

namespace MellorsApp\View\Helper;

/**
 * Helper for minifying stylesheets with crushCSS
 *
 * @package    Zend_View
 * @subpackage Helper
 */

class headScript extends \Zend\View\Helper\HeadScript
{    
    protected $_minifiedScripts = array();
    protected $_minifier;
    protected $_fileTimes = array();
    protected $_scriptName = '';
    
    public function __construct() 
    {
        parent::__construct();
    }
    
    public function minify($input)
    {
        return $input;
    }
    
    public function __call($method, $args)
    {            
        $fileMethods = array('setFile', 'appendFile', 'prependFile');
        if(in_array($method, $fileMethods)){
            $this->_fileTimes[] = filemtime($this->getFilePath($args[0]));            
            $this->_scriptName .= basename($args[0]) . '-';
        }        
        return parent::__call($method, $args);
    }
    
    public function getFilePath($basePath)
    {
	$viewPath = '/^'.preg_quote($this->getView()->basePath(), '/').'/';
        $filePath = 
            //As index.php creates via chdir a root directory, this way is more consistens that DOCUMENT_ROOT
            getcwd() . '/public' .
            //Get base path without root directory
            preg_replace($viewPath, '', $basePath);
        return $filePath;
       
    }
   
    public function toString($indent = null) {
        
        if($this->_scriptName == ''){
            return parent::toString($indent);
        }
        
        $fileName = realpath('') . '/public/js/minified/' . md5($this->_scriptName) . '.js';
        
        $newFile = false;
               
        if(!file_exists($fileName)){
            if(!file_exists(realpath('public/js/minified/'))){
                mkdir(realpath('public/js/minified/'), 0700, true);
            }
            $fp = fopen($fileName, 'w');
            fclose($fp);
            $newFile = true;
        }
        
        if(filemtime($fileName) < max($this->_fileTimes) || $newFile){
            $this->_minifiedScripts = array();            

            $this->getContainer()->ksort();
            foreach ($this as $item) {
                //echo $item->attributes['src'] . '               ';
                $content = file_get_contents($this->getFilePath($item->attributes['src']));
                $this->_minifiedScripts[] = $this->minify($content);
            }
            $fp = fopen($fileName, 'w');
            fwrite($fp, implode($this->_minifiedScripts));            
            fclose($fp);         
        }
        
        parent::setFile($this->getView()->basePath('js/minified/' . md5($this->_scriptName) . '.js'));

        $this->_scriptName = '';
        
        return parent::toString($indent);
    }

}

?>