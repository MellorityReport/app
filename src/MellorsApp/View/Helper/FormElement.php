<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * @package   Zend_View
 */

namespace MellorsApp\View\Helper;

/**
 * Helper for minifying stylesheets with crushCSS
 *
 * @package    Zend_View
 * @subpackage Helper
 */

class FormElement extends \Zend\Form\View\Helper\FormElement
{    
    
    public function render(\Zend\Form\ElementInterface $element) {
        return 
            '<dd' . (($element->getAttribute('type')) ? (' class="' . $element->getAttribute('type') . '"') : ('')) . '>' .
            parent::render($element) .
            '</dd>';
    }
}

?>