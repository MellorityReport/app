<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * @package   Zend_View
 */

namespace MellorsApp\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Helper for minifying stylesheets with crushCSS
 *
 * @package    Zend_View
 * @subpackage Helper
 */

class Attribute extends AbstractHelper
{
    
    public function __invoke($id, $value)
    {
        
        $val = $this->getView()->escapeHtmlAttr(
                preg_replace(array("/ .*[üäö].* /U", "/[^A-Za-z0-9]/"), '-',  
                strtolower($value)));
        $val = implode("-",array_slice(explode("-", $val), 0, 5));
        $str = $id . '="' . $val .'"';
        
        return $str;
    }
 
}
