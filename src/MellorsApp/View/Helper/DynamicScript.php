<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * @package   Zend_View
 */

namespace MellorsApp\View\Helper;

/**
 * Helper for minifying stylesheets with crushCSS
 *
 * @package    Zend_View
 * @subpackage Helper
 */

class DynamicScript extends \Zend\View\Helper\AbstractHelper
{    
    protected $_jsService;
    
    public function setJavascriptService($service)
    {
        $this->_jsService = $service;
    }
    
    public function __invoke() 
    {
        return $this;
    }
    
    public function append($script, $id = false){
        $this->_jsService->append($script, $id);
        return $this;
    }
    
    public function __toString() {
        return $this->_jsService->getScript();
    }

}

?>