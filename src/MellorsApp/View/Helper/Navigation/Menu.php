<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace MellorsApp\View\Helper\Navigation;

use RecursiveIteratorIterator;
use Zend\Navigation\AbstractContainer;
use Zend\Navigation\Page\AbstractPage;
use Zend\View;
use Zend\View\Exception;

/**
 * Helper for rendering menus from navigation containers
 */
class Menu extends \Zend\View\Helper\Navigation\Menu
{

    protected $_urlService;

    protected $_parentContainer;
    
    public function renderMenu($container = null, array $options = array())
    {
        $normalizedOptions = $this->normalizeOptions($options);
        $this->_parentContainer = '.' .$normalizedOptions['ulClass'];
        
        return parent::renderMenu($container, $options);
    }
    
    public function setUrlService($urlService)
    {
        $this->_urlService = $urlService;
        return $this;
    }
    public function htmlify(AbstractPage $page, $escapeLabel = true, $addClassToListItem = false)
    {
        $this->_urlService->
            setUrl($page->getHref())->
            setIdentifier()->
            setParams(['controller' => $page->getController(), 'action' => $page->getAction()])->
            setParentContainer($this->_parentContainer)->
            process();
        return parent::htmlify($page, $escapeLabel, $addClassToListItem);
    }

    
    

}
