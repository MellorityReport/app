<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_View
 * @subpackage Helper
 * @copyright  Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */

/**
 * @namespace
 */
namespace MellorsApp\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Helper for retrieving an asset URL.
 *
 * @package    Zend_View
 * @subpackage Helper
 * @copyright  Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class AssetUrl extends AbstractHelper
{
    /**
     * Base URL for assets.
     *
     * @var string
     */
    protected $assetBaseUrl;
    
    /**
     * Suffix for assets.
     *
     * @var string
     */
    protected $suffix;
    
    /**
     * Detects if browser cached should be used
     * 
     * @var boolean
     */
    protected $cache = true;

    /**
     * Returns the URL for an asset.
     *
     * If a suffix was set, it will be appended as query parameter to the URL.
     *
     * @param  string  $file
     * @param  boolean $omitSuffix
     * @return string
     */
    public function __invoke($file = false, $omitSuffix = false)
    {
        if (null === $this->assetBaseUrl) {
            return $this->getView()->basePath($file);
        }
        
        if(!$file){
            return $this;
        }

        $url = $this->assetBaseUrl . '/' . ltrim($file, '/');
        
        if (!$omitSuffix && null !== $this->suffix) {
            if (strpos($url, '?') === false) {
                $url .= '?' . $this->suffix;
            } else {
                $url .= '&' . $this->suffix;
            }
        }

        return $url;
    }

    /**
     * Set the asset base URL.
     *
     * @param  string $assetBaseUrl
     * @return AssetUrl
     */
    public function setAssetBaseUrl($assetBaseUrl)
    {
        $this->assetBaseUrl = rtrim($assetBaseUrl, '/');
        return $this;
    }

    /**
     * Set a suffix for assets.
     *
     * @param  string $suffix
     * @return AssetUrl
     */
    public function setSuffix($suffix)
    {
        $this->suffix = $suffix;
        return $this;
    }
    
    /**
     * Sets brose
     * @param type $cache
     * @return \MellorsApp\View\Helper\AssetUrl
     */
    public function setCache($cache = false){
        if(!$cache){
            $this->cache = false;
            $this->setSuffix('bust=' . time());
        }
        return $this;
    }
    
    /**
     * Returs, if cache is set to true
     * @return boolean
     */
    public function getCache(){
        return $this->cache;
    }
}
