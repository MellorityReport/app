<?php
/**
 * OBADJA - Frontend Application for yocondo.de (http://de.yocondo.de/)
 * 
 * @link      http://github.com/mellors/obadja for the canonical source repository
 * @author    Marcel Mellor <info@mellors.de>
 * @copyright Copyright (c) 2012 Mellors (http://www.mellors.de)
 */

namespace MellorsApp;

use \Zend\Mvc\MvcEvent;
use  \Zend\Mvc\ModuleRouteListener;

require_once 'src/MellorsApp/AbstractModule.php';

class Module extends AbstractModule
{
    
    public function getDir() 
    {
        return __DIR__;
    }
    
    public function getNamespace() 
    {
        return __NAMESPACE__;
    }    
    
    public function onBootstrap(\Zend\Mvc\MvcEvent $event) {
        
        $application = $event->getApplication();
        $serviceManager = $application->getServiceManager();
        $eventManager  = $application->getEventManager();

        $target = $event->getTarget();        
        $target->getEventManager()->attach(
            $target->getServiceManager()->get('ZfcRbac\View\Strategy\UnauthorizedStrategy')
        );
    
        //Set translator
        $viewHelper = $serviceManager->get('ViewHelperManager');
        $config = $serviceManager->get('Config');
        $viewHelper->setAlias('_', 'translate');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($serviceManager->get('mvctranslator'));

        
       
        //Set cache
        $viewHelper->get('assetUrl')->setAssetBaseUrl(
                $viewHelper->get('basePath')->__invoke() .
                $serviceManager->get('asset_path')
        )->setCache($config['application_env'] == 'development' ? false : true);
     
        //Set navigation menu helper
        $pm = $serviceManager->get('ViewHelperManager')->get('Navigation')->getPluginManager();
        $pm->setFactory('menu', function() use ($serviceManager){
            $helper = new \MellorsApp\View\Helper\Navigation\Menu();
            $helper->setUrlService($serviceManager->get('url'));  
            return $helper;
        });
        
        $eventManager->getSharedManager()->attach(
            'Zend\View\Helper\Navigation\AbstractHelper', 
            'isAllowed',
            array('\MellorsApp\Listener\RbacListener', 'accept')
        );
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        
        parent::onBootstrap($event);
    }
    
    public function getViewHelperConfig()
    {
            return array(
                'factories' => array(
                    'DynamicScript' => function($helpers){
                        $helper = new \MellorsApp\View\Helper\DynamicScript();
                        $helper->setJavascriptService($helpers->getServiceLocator()->get('javascript'));
                        return $helper;
                        },
                   'Url' => function($helpers){
                        $helper = new \MellorsApp\View\Helper\Url();
                        $helper->setRouter($helpers->getServiceLocator()->get('router'));
                        $helper->setUrlService($helpers->getServiceLocator()->get('url'));
                        return $helper;
                   }
                )        
                        
            );
    }
    
    public function makeSeoRoutes(MvcEvent $e){

        echo 'test';
    }
    
    public function getControllerPluginConfig()
    {
          return array('factories' => array(
                'Url' => function($helpers){
                      $urlPlugin = new \MellorsApp\Controller\Plugin\Url();
                      $urlPlugin->setUrlService(
                          $helpers->getServiceLocator()->get('url')
                      );
                      return $urlPlugin;
                  }          
              )
          );
        
    }

}
